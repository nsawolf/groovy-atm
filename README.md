# Atm Project
The purpose of this project is to simulate an ATM.
A user is able to deposit, withdraw, and view their balance once they are 
successfully logged into the system.  A successful login requires a valid
card number and a valid pin number.  A user is allowed three attempts to enter valid
credentials.  If all attempts fail, the application terminates.




##Running locally

### Build the project
To build the project from the command line execute from within the project directory
`./gradlew clean build`

### Run tests for the project
`./gradlew test`

### Start up the docker container 
With this project there is a docker container which houses a postgres database.
This database holds the account information for users which are able to log into 
the project.
To start the docker container make sure the docker desktop is installed on your system.
Once docker is installed, from the root directory of the project, within a terminal window,
execute the following command `docker-compose -f atm-stack.yml up -d`, this will start up
a postgres instance, and an Adminer instance.
The Adminer instance can be leveraged to manage the postgres instance via a simple UI.

#### Load SQL data
To load the SQL data before starting up the application, a user has 2 options:
1. Docker exec:
   1. execute `docker ps` to list the service running
   2. copy the `container-name` for the postgres service
   3. From a terminal window within the root directory of the project execute the following (replacing `<container-name>` with value copied from previous step): `cat ./resources/data/dataInitialize.sql | docker exec -i <container-name> psql -U atm_user -d atm_database`
2. Adminer UI:
   1. Open a browser
   2. Navigate to `http://localhost:8080`
   3. Select `PostgresSQL` as the database type, then enter the credentials user: `atm_user` pwd: `l3tm31n` database: `atm_database`
   4. Once authenticated, navigate to the `SQL Command` menu item on the left.
   5. Copy the contents of `src/main/resource/data/dataInitialize.sql`
   6. execute the script

### Running the service locally
To run the service locally, after the project has been built successfully, and the database is set up,
from the command line at the root of the project,
execute:  `./gradlew bootRun`
