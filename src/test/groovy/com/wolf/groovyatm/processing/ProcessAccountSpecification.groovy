package com.wolf.groovyatm.processing

import com.wolf.groovyatm.auth.ValidateInput
import com.wolf.groovyatm.models.Account
import com.wolf.groovyatm.service.AccountServiceImpl
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class ProcessAccountSpecification extends Specification {

    private final AccountServiceImpl mockAccountService = Mock()
    private final ValidateInput validateInput = Mock()
    private final ProcessAccount processAccount = new ProcessAccount(mockAccountService, validateInput)

    @Shared
    Account testAccount = new Account(id: 1, cardNumber: 9999444455558888, pinNumber: 2222, balance: 50.00, name: "Sam Sample")

    def 'getAccountInfo returns a valid account'() {
        given:
        mockAccountService.findById(_) >> testAccount

        when:
        Account result = processAccount.getAccountInfo(1)

        then:
        result != null
        result == testAccount
    }

    @Unroll
    def 'successfulWithdrawlFromAccount returns expected result'() {
        given:
        validateInput.isValidDollarAmount(_) >> true

        expect:
        processAccount.successfulWithdrawlFromAccount(mockAmount, testAccount) == expected

        where:
        mockAmount || expected
        "10.00"    || true
        "100"      || false
    }

    @Unroll
    def 'successfulDepositFromAccount returns expected result'() {
        given:
        validateInput.isValidDollarAmount(_) >>> validAmt

        expect:
        processAccount.successfulDepositToAccount(mockAmount, testAccount) == expected

        where:
        mockAmount | validAmt || expected
        "10.00"    | true     || true
        "100"      | true     || true
        "se"       | false    || false
    }

    @Unroll
    def 'userWantsBalance returns expected result'() {
        expect:
        processAccount.userWantsBalance(mockInput) == expected

        where:
        mockInput || expected
        "0"       || true
        null      || false
        "2"       || false
        "1"       || false
    }

    @Unroll
    def 'userWantsToWithdrawFromAccount returns expected result'() {
        expect:
        processAccount.userWantsToWithdrawFromAccount(mockInput ) == expected

        where:
        mockInput || expected
        "0"       || false
        null      || false
        "2"       || false
        "1"       || true
    }

    @Unroll
    def 'userWantsToDepositToAccount returns expected result'() {
        expect:
        processAccount.userWantsToDepositToAccount(mockInput ) == expected

        where:
        mockInput || expected
        "0"       || false
        null      || false
        "2"       || true
        "1"       || false
    }
}
