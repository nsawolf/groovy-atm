package com.wolf.groovyatm.auth

import spock.lang.Specification
import spock.lang.Unroll

class ValidateCardSpec extends Specification {

    private final ValidateInput validateCard = new ValidateInput()

    @Unroll
    def 'isValidCardNumber evaluates card as expected'() {
        expect:
        validateCard.isValidCardNumber(mockCardNumber) == expected

        where:
        mockCardNumber     || expected
        null               || false
        "ssee"             || false
        "123489f90920987"  || false
        "4433"             || false
        "1111222233334444" || true
        ""                 || false
    }

    @Unroll
    def 'isValidPinNumber evaluates pin as expected'() {
        expect:
        validateCard.isValidPinNumber(mockPinNumber) == expected

        where:
        mockPinNumber || expected
        null          || false
        ""            || false
        "2"           || false
        "rr3e"        || false
        "cats"        || false
        "orange"      || false
        "8888"        || true
    }

    @Unroll
    def 'isValidDollarAmount evalutes dollar amount as expected'() {
        expect:
        validateCard.isValidDollarAmount(mockDollarAmount) == expected

        where:
        mockDollarAmount || expected
        null             || false
        ""               || false
        "eew"            || false
        "100"            || true
        "30.33"          || true
        "90.9"           || true
        "80."            || false
        "9e0"            || false
        "5Y2"            || false
        "ee99"           || false
    }
}
