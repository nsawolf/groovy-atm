//package repo
//
//import com.wolf.groovyatm.models.Account
//import com.wolf.groovyatm.repo.AccountRepository
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
//import org.springframework.test.context.ContextConfiguration
//import spock.lang.Specification
//
//@ContextConfiguration(classes=AccountRepository.class)
//@DataJpaTest
//class AccountRepositoryIntegrationSpec extends Specification {
//
//    @Autowired
//    private AccountRepository accountRepository
//
//    def 'findByCardNumberAndPinNumber'() {
//        when:
//        Account account = accountRepository.findByCardNumberAndPinNumber(5555444433332222 as BigInteger, 6644)
//        then:
//        account.balance == 500
//    }
//}
