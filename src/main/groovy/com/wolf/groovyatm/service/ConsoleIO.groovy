package com.wolf.groovyatm.service

interface ConsoleIO {
    String getConsoleInput()
    void generateConsoleOutput(String msg, Object... args)
}