package com.wolf.groovyatm.service

import com.wolf.groovyatm.models.Account

interface AccountService {

    Account findByCardNumberAndPinNumber(BigInteger cardNumber, Integer pinNumber)

    Account saveAccount(Account account)

    Account findById(Integer id)
}
