package com.wolf.groovyatm.service

import org.springframework.stereotype.Service

@Service
class ConsoleIOImpl implements ConsoleIO {
    protected static final InputStreamReader lineNumber = new InputStreamReader(System.in);

    @Override
    String getConsoleInput(){
        BufferedReader br = new BufferedReader(lineNumber)
        return br.readLine();
    }

    @Override
    void generateConsoleOutput(String msg, Object... args) {
        System.out.printf(msg, args);
    }
}
