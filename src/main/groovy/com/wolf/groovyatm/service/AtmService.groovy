package com.wolf.groovyatm.service

import com.wolf.groovyatm.auth.ValidateCredentials
import com.wolf.groovyatm.models.Account
import com.wolf.groovyatm.models.Message
import com.wolf.groovyatm.processing.ProcessAccount
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class AtmService {

    private final ConsoleIO consoleIO
    private final ValidateCredentials validateCredentials
    private final ProcessAccount processAccount
    private int MAX_TRIES = 3

    @Autowired
    AtmService(ConsoleIOImpl consoleIO, ValidateCredentials validateCredentials, ProcessAccount processAccount) {
        this.consoleIO = consoleIO
        this.validateCredentials = validateCredentials
        this.processAccount = processAccount
    }

    void runAtmService() {
        String quitAtm = "9"
        Integer attempts = 0
        boolean validUser = false
        String cardNumber
        String pinNumber
        Account account = null

        try {
            do {
                consoleIO.generateConsoleOutput(Message.CARDNUMBER_PROMPT.messageText)
                cardNumber = consoleIO.getConsoleInput()
                consoleIO.generateConsoleOutput(Message.PINNUMBER_PROMPT.messageText)
                pinNumber = consoleIO.getConsoleInput()
                account = validateCredentials.accountWithCredentials(cardNumber, pinNumber)
                validUser = account != null
                if (!validUser) {
                    attempts++
                    consoleIO.generateConsoleOutput(Message.INVALID_CREDENTIALS.messageText + "\n")
                }
            } while(!validUser && attempts < MAX_TRIES)
            if(attempts >= MAX_TRIES) {
                System.exit(0)
            }
            do {
                if (validUser) {
                    consoleIO.generateConsoleOutput(Message.VALID_INPUT_PROMPT.messageText)
                    String nextStep = consoleIO.getConsoleInput()
                    if (nextStep != quitAtm) {
                        if (processAccount.userWantsBalance(nextStep)) {
                            consoleIO.generateConsoleOutput(Message.BALANCE_PROMPT.messageText + account.balance)
                        } else if (processAccount.userWantsToWithdrawFromAccount(nextStep) || processAccount.userWantsToDepositToAccount(nextStep)) {
                            consoleIO.generateConsoleOutput(Message.AMOUNT_PROMPT.messageText)
                            String amount = consoleIO.getConsoleInput()
                            if (processAccount.userWantsToWithdrawFromAccount(nextStep)) {
                                if (processAccount.successfulWithdrawlFromAccount(amount, account)) {
                                    consoleIO.generateConsoleOutput(Message.SUCCESSFUL_TRANSACTION.messageText)
                                    Account updatedAccount = processAccount.getAccountInfo(account.id)
                                    consoleIO.generateConsoleOutput(Message.BALANCE_PROMPT.messageText + updatedAccount.balance)

                                } else {
                                    consoleIO.generateConsoleOutput(Message.UNSUCCESSFUL_TRANSACTION.messageText)
                                }
                            } else if (processAccount.userWantsToDepositToAccount(nextStep)) {
                                if (processAccount.successfulDepositToAccount(amount, account)) {
                                    consoleIO.generateConsoleOutput(Message.SUCCESSFUL_TRANSACTION.messageText)
                                    Account updatedAccount = processAccount.getAccountInfo(account.id)
                                    consoleIO.generateConsoleOutput(Message.BALANCE_PROMPT.messageText + updatedAccount.balance)
                                } else {
                                    consoleIO.generateConsoleOutput(Message.UNSUCCESSFUL_TRANSACTION.messageText)
                                }
                            }
                        } else { System.exit(0)}
                    } else {
                        System.exit(0)
                    }
                }
            }
            while (consoleIO.getConsoleInput() != quitAtm)
        } catch (e) {
            e.printStackTrace()
        }
    }
}
