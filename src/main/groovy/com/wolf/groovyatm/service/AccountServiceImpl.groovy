package com.wolf.groovyatm.service

import com.wolf.groovyatm.models.Account
import com.wolf.groovyatm.repo.AccountRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AccountServiceImpl implements AccountService{

    private final AccountRepository repository

    @Autowired
    AccountServiceImpl(AccountRepository accountRepository) {
        repository = accountRepository
    }


    @Override
    Account findByCardNumberAndPinNumber(BigInteger cardNumber, Integer pinNumber) {
        return repository.findByCardNumberAndPinNumber(cardNumber, pinNumber)
    }

    @Override
    Account saveAccount(Account account) {
        return repository.save(account)
    }

    @Override
    Account findById(Integer id) {
        return repository.findById(id).get()
    }
}
