package com.wolf.groovyatm.auth

import org.springframework.stereotype.Component

@Component
class ValidateInput {

    boolean isValidCardNumber(String cardNumber) {
        return cardNumber?.matches('\\d{16}')
    }

    boolean isValidPinNumber(String pinNumber) {
        return pinNumber?.matches('\\d{4}')
    }

    boolean isValidDollarAmount(String dollarAmt) {
        return dollarAmt?.matches('[^a-zA-Z]+\\d+(.\\d+)?')
    }
}
