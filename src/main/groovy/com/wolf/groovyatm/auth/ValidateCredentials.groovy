package com.wolf.groovyatm.auth

import com.wolf.groovyatm.models.Account
import com.wolf.groovyatm.service.AccountService
import com.wolf.groovyatm.service.AccountServiceImpl
import org.springframework.stereotype.Component

@Component
class ValidateCredentials {

    private final ValidateInput validateInput
    private final AccountService accountService

    ValidateCredentials(ValidateInput validateInput, AccountServiceImpl accountService) {
        this.validateInput = validateInput
        this.accountService = accountService
    }

    Account accountWithCredentials(String cardNumber, String pinNumber) {
        if (validateInput.isValidCardNumber(cardNumber) && validateInput.isValidPinNumber(pinNumber)) {
            return accountService.findByCardNumberAndPinNumber(new BigInteger(cardNumber), Integer.parseInt(pinNumber))
        }
        return null
    }
}
