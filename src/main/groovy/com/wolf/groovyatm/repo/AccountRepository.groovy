package com.wolf.groovyatm.repo

import com.wolf.groovyatm.models.Account
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface AccountRepository extends JpaRepository<Account, Integer> {

    @Query("SELECT a FROM Account a WHERE a.cardNumber = ?1 AND a.pinNumber = ?2")
    Account findByCardNumberAndPinNumber(BigInteger cardNumber, Integer pinNumber)

    Optional<Account> findById(Integer id)
}
