package com.wolf.groovyatm.models

enum Message {

    CARDNUMBER_PROMPT("Please enter in your 16 digit card number, only the numbers: "),
    PINNUMBER_PROMPT("Please enter your 4 digit pin: "),
    VALID_INPUT_PROMPT("To see balance press 0, to withdrawl press 1, to deposit press 2, to quit press 9"),
    BALANCE_PROMPT("Your balance is: "),
    AMOUNT_PROMPT("Please enter the amount to adjust: "),
    INVALID_CREDENTIALS("Invalid credentials.  Please try again."),
    INVALID_VALUE("Invalid entry.  Please try again, or press 9 to quit."),
    SUCCESSFUL_TRANSACTION("Your transaction was successful.  Your new balance is: "),
    UNSUCCESSFUL_TRANSACTION("Sorry, your transaction was not able to be processed.  "),
    CONTINUE_SESSION("To continue press enter")
    String messageText

    private Message(String messageText) {
        this.messageText = messageText
    }

}