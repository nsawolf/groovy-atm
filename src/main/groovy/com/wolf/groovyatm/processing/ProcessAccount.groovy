package com.wolf.groovyatm.processing

import com.wolf.groovyatm.auth.ValidateInput
import com.wolf.groovyatm.models.Account
import com.wolf.groovyatm.service.AccountService
import com.wolf.groovyatm.service.AccountServiceImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class ProcessAccount {
    private final AccountService accountService
    private final ValidateInput validateInput

    @Autowired
    ProcessAccount(AccountServiceImpl accountService, ValidateInput validateInput) {
        this.accountService = accountService
        this.validateInput = validateInput
    }

    Account getAccountInfo(Integer id) {
        return accountService.findById(id)
    }

    boolean successfulWithdrawlFromAccount(String withdrawlAmt, Account account) {
        if (validateInput.isValidDollarAmount(withdrawlAmt)) {
            Double withdraw = Double.parseDouble(withdrawlAmt)
            if (withdraw <= account.balance) {
                account.balance -= withdraw
                accountService.saveAccount(account)
                return true
            }
        }
        return false
    }

    boolean successfulDepositToAccount(String depositAmt, Account account) {
        if (validateInput.isValidDollarAmount(depositAmt)) {
            account.balance += Double.parseDouble(depositAmt)
            accountService.saveAccount(account)
            return true
        }
        return false
    }

    Boolean userWantsBalance(String userInput) {
        return userInput == "0"
    }

    Boolean userWantsToWithdrawFromAccount(String userInput) {
        return userInput == "1"
    }

    Boolean userWantsToDepositToAccount(String userInput) {
        return userInput == "2"
    }
}
