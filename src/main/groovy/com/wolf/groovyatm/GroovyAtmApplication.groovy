package com.wolf.groovyatm

import com.wolf.groovyatm.auth.ValidateCredentials
import com.wolf.groovyatm.models.Account
import com.wolf.groovyatm.models.Message
import com.wolf.groovyatm.processing.ProcessAccount
import com.wolf.groovyatm.service.AtmService
import com.wolf.groovyatm.service.ConsoleIO
import com.wolf.groovyatm.service.ConsoleIOImpl
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@Slf4j
@SpringBootApplication
class GroovyAtmApplication implements CommandLineRunner {

    private final AtmService atmService


    @Autowired
    GroovyAtmApplication(AtmService atmService) {
        this.atmService = atmService
    }

    static void main(String[] args) {
        SpringApplication.run(GroovyAtmApplication, args)
    }

    @Override
    void run(String... args) throws Exception {
        atmService.runAtmService()
    }
}
